function schedule() {
    var timepiece = new Date();
    var clock = document.getElementById('clock');
    clock.innerHTML = timepiece.toLocaleTimeString();
    clock.setAttribute('style', 'color: #55caf8;');
    if (timepiece.getMinutes() % 2 === 0) { 
        clock.setAttribute('style', 'color: #5ce47e;'); 
    }
}
setInterval(schedule, 1000);