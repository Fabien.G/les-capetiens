var slider = document.getElementById("box");
var image = ["img/louis-ix-01", "img/louis-ix-02"];
var b = image.length;

function nextImg() {
  if (b < image.length) {
    b = b + 1;
  } else {
    b = 1;
  }
  slider.innerHTML = "<img src=" + image[b - 1] + ".jpg>";
}

function prevImg() {
  if (b < image.length + 1 && b > 1) {
    b = b - 1;
  } else {
    b = image.length;
  }
  slider.innerHTML = "<img src=" + image[b - 1] + ".jpg>";
}
